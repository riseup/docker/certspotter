FROM debian:buster as build
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    build-essential pkg-config golang-go git ca-certificates openvpn \
    && rm -rf /var/lib/apt/lists/*
ENV GOPATH=/go
WORKDIR $GOPATH
RUN go get software.sslmate.com/src/certspotter/cmd/certspotter
RUN strip $GOPATH/bin/certspotter

FROM registry.git.autistici.org/ai3/docker/chaperone-base
COPY --from=build /go/bin/certspotter /usr/local/bin/certspotter
COPY chaperone.d/ /etc/chaperone.d

CMD ["/usr/local/bin/chaperone"]

